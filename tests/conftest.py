import pytest

from api import create_app


@pytest.fixture
def app():
    app = create_app({
        'TESTING': True,
        'CACHE_TYPE': 'null',
        'CACHE_NO_NULL_WARNING': True,
    })

    return app
