"""Test status 'model'"""
import pytest

from api.status.status import LEVEL_MAP, status


_status = [
    (22950, 'red', 'Carter B is the Day Operator', 'CLB', None, 1564056071),
    (22949, 'yellow', 'Carter B is the Day Operator', 'CLB', None, 1563969391),
    (22948, 'lightgreen', 'Carter B is Day Operator', 'CLB', None, 1563883155)
]


@pytest.fixture
def status_mock(mocker):
    yield mocker.patch('api.status.db.get_status', return_value=_status)


@pytest.mark.usefixtures('status_mock')
class TestStatus:

    def test_returns_list(self):
        assert isinstance(status(), list)

    def test_list_contains_dicts(self):
        entries = status()

        assert len(entries) != 0
        for entry in entries:
            assert isinstance(entry, dict)

    def test_status_contain_proper_keys(self):
        entries = status()

        assert len(entries) != 0
        for entry in entries:
            assert set(entry.keys()) == \
                {'date', 'level', 'message', 'author'}

    def test_status_has_proper_values(self):
        entries = status()

        assert len(entries) != 0
        for x, entry in enumerate(entries):
            assert entry['date'] == _status[x][5]
            assert entry['level'] == LEVEL_MAP[_status[x][1]]
            assert entry['message'] == _status[x][2]
            assert entry['author'] == _status[x][3]
