"""Tests for status.status_ep endpoint"""

from flask import url_for
import pytest


_content = [
    {'date': 1564056071,
     'level': 'Down',
     'message': 'Status message',
     'author': 'PS'},
    {'date': 1564055071,
     'level': 'Warning',
     'message': 'Warning message',
     'author': 'MAC'
     }
]


@pytest.fixture
def status_mock(mocker):
    yield mocker.patch('api.status.status', return_value=_content)


@pytest.mark.usefixtures('client_class', 'status_mock')
class TestStatusStatusEndpoint:

    def test_exists(self):
        assert self.client.get(url_for('status.status_ep')).status_code == 200

    def test_content_type_json(self):
        assert self.client.get(url_for('status.status_ep')).is_json

    def test_returns_json(self):
        assert \
            self.client.get(url_for('status.status_ep')).get_json() == \
            _content
