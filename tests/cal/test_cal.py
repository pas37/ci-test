"""Tests for cal.events_ep endpoint"""

from flask import url_for
import pytest


_content = [
    {'test': 'test1'},
    {'test': 'test2'}
]


@pytest.fixture
def events_mock(mocker):
    yield mocker.patch('api.cal.events', return_value=_content)


@pytest.mark.usefixtures('client_class', 'events_mock')
class TestCalEventsEndpoint:

    def test__exists(self):
        assert self.client.get(url_for('cal.events_ep')).status_code == 200

    def test_content_type_json(self):
        assert self.client.get(url_for('cal.events_ep')).is_json

    def test_returns_json(self, mocker):
        assert self.client.get(url_for('cal.events_ep')).get_json() == _content
