"""Test events 'model'"""
import pytest

from api.cal.events import events

_events = [
    (1564410600, 1564412400, 0, 'MacCHESS Meeting', ''),
    (1564412400, 1564416000, 0, 'MSN-C meeting', 'W301'),
    (1564509600, 1564513200, 0, 'CLASSE Resource Planning', 'W301'),
    (1564545600, 1564632000, 1, 'Workday Hourly Timecard Approval', '')
]


@pytest.fixture
def event_mock(mocker):
    yield mocker.patch('api.cal.db.get_events', return_value=_events)


@pytest.mark.usefixtures('event_mock')
class TestEvents:

    def test_returns_list(self):
        assert isinstance(events(), list)

    def test_list_contains_dicts(self, event_mock):
        for event in events():
            assert isinstance(event, dict)

    def test_events_contain_proper_keys(self):
        for event in events():
            assert set(event.keys()) == \
                {'start', 'end', 'allday', 'name', 'location'}

    def test_event_have_proper_values(self):
        for x, event in enumerate(events()):
            assert event['start'] == _events[x][0]
            assert event['end'] == _events[x][1]
            assert event['allday'] == bool(_events[x][2])
            assert event['name'] == _events[x][3]
            assert event['location'] == _events[x][4]
