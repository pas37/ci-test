#
# simple makefile to automate repeatative tasks
#

DESTDIR=$(shell echo "/tmp${PWD}")

#
# simple taks
#

.PHONY: clean cleanall cleancache coverage run tests

cleanall: clean cleancache

clean:
	find . -name "*~" -delete
	rm -rf .coverage htmlcov/
	rm -rf pytest.xml .pytest_cache/
	rm -rf release.tar requirements.txt venv

cleancache:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete

coverage:
	pytest --cov=api --cov-branch --cov-report=term --cov-report=html
	firefox htmlcov/index.html

run:
	FLASK_ENV=developement FLASK_APP=api flask run -h 0.0.0.0

tests:
	ptw


#
# build release
#

release.tar: venv
	mkdir -p $(DESTDIR)
	cp -r api venv $(DESTDIR)
	mkdir $(DESTDIR)/instance
	tar -cf release.tar -C /tmp .$(shell pwd)
	rm -r $(DESTDIR)

requirements.txt: Pipfile Pipfile.lock
	pipenv lock -r >requirements.txt

venv: requirements.txt
	python3 -m venv venv
	./venv/bin/pip install -r requirements.txt

