from flask import Flask
from flask_caching import Cache
from flask_cors import CORS


default_config = {
    'SECRET_KEY': 'dev',

    'CACHE_TYPE': 'simple',
    'CACHE_DEFAULT_TIMEOUT': 300
}

cache = Cache()
cors = CORS()


def create_app(test_config=None):
    # create app and give default config
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(default_config)

    if test_config is None:
        # load instance config
        app.config.from_pyfile('misc_api.cfg', silent=True)
    else:
        # load test config
        app.config.from_mapping(test_config)

    # initialize extensions
    cache.init_app(app)
    cors.init_app(app)

    # simple index route
    app.add_url_rule('/', 'index', lambda: 'Misc API server')

    # add blueprint routes
    from api.cal import cal_bp
    from api.status import status_bp

    app.register_blueprint(cal_bp, url_prefix='/api/cal')
    app.register_blueprint(status_bp, url_prefix='/api/status')

    return app
