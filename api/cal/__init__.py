from flask import Blueprint, jsonify

from api import cache
from api.cal.events import events


# create blueprint
cal_bp = Blueprint('cal', __name__)


# routes

@cal_bp.route('/events')
@cache.cached()
def events_ep():
    return jsonify(events())
