"""Connect and run queries against the calendar database"""
from contextlib import contextmanager
from datetime import datetime, timedelta
from urllib.parse import urlparse

import MySQLdb
from flask import current_app


@contextmanager
def _connect():
    if not current_app.config.get('DB_CAL_URL'):
        raise ValueError('DB_CAL_URL not defined')

    pars = {}
    url = urlparse(current_app.config['DB_CAL_URL'])

    pars['db'] = url.path[1:]
    pars['host'] = url.hostname
    if url.port:
        pars['port'] = url.port
    if url.username:
        pars['user'] = url.username
    if url.password:
        pars['passwd'] = url.password

    connection = MySQLdb.connect(**pars)
    yield connection
    connection.close()


def get_events(start=None, end=None):
    # start defaults to today
    if start is None:
        now = datetime.now()
        start = datetime(now.year, now.month, now.day)

    # end defaults to 7 days from start
    if end is None:
        end = start + timedelta(days=7)

    s_date = int(start.timestamp())
    e_date = int(end.timestamp())

    # execute query
    with _connect() as db:
        cursor = db.cursor()
        results = []

        cursor.execute(
            "SELECT start,end,allday,summary,location FROM xoops22_pical_event"
            + " WHERE admission > 0 AND class='public' AND "
            + " ((start BETWEEN %s AND %s) or (end BETWEEN %s AND %s) or "
            + "  (start < %s and end > %s)) ORDER BY start,end;",
            (s_date, e_date, s_date, e_date, s_date, e_date))

        for row in cursor:
            results.append(row)

    # return results
    return results
