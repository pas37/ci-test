"""events 'model' - return dictionary of events"""
import api.cal.db


def events():
    event_list = []

    # convert database tuple list into dict list
    for event_tuple in api.cal.db.get_events():
        new_event = {'start': event_tuple[0],
                     'end': event_tuple[1],
                     'allday': bool(event_tuple[2]),
                     'name': event_tuple[3],
                     'location': event_tuple[4]}
        event_list.append(new_event)

    return event_list
