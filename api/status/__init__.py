from flask import Blueprint, jsonify

from api import cache
from api.status.status import status


# create blueprint
status_bp = Blueprint('status', __name__)


# routes

@status_bp.route('/status')
@cache.cached(timeout=60)
def status_ep():
    return jsonify(status())
