"""Connect and run queries against the calendar database"""
from contextlib import contextmanager
from urllib.parse import urlparse

import MySQLdb
from flask import current_app


@contextmanager
def _connect():
    if not current_app.config.get('DB_STATUS_URL'):
        raise ValueError('DB_STATUS_URL not defined')

    pars = {}
    url = urlparse(current_app.config['DB_STATUS_URL'])

    pars['db'] = url.path[1:]
    pars['host'] = url.hostname
    if url.port:
        pars['port'] = url.port
    if url.username:
        pars['user'] = url.username
    if url.password:
        pars['passwd'] = url.password

    connection = MySQLdb.connect(**pars)
    yield connection
    connection.close()


def get_status():
    # get last 24 hours of messages, or last message
    with _connect() as db:
        cursor = db.cursor()
        results = []

        num = cursor.execute(
            "SELECT *,UNIX_TIMESTAMP(Date_Created) AS Timestamp "
            + "FROM StatusNotes WHERE Date_Created > NOW() - INTERVAL 24 HOUR "
            + "ORDER BY Date_Created DESC;")
        if num == 0:
            cursor.execute(
                "SELECT *,UNIX_TIMESTAMP(Date_Created) AS Timestamp "
                + "FROM StatusNotes ORDER BY Date_Created DESC LIMIT 1;")

        for row in cursor:
            results.append(row)

    return results
