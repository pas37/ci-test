"""status 'model' - return dictionary of status"""
import api.status.db

LEVEL_MAP = {'red': 'Down', 'yellow': 'Alert', 'lightgreen': 'Normal'}


def status():
    entries = []

    for entry in api.status.db.get_status():
        entries.append({
            'date': entry[5],
            'level': LEVEL_MAP[entry[1]],
            'message': entry[2],
            'author': entry[3]
        })
    return entries
