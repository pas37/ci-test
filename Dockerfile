FROM gitlab01.classe.cornell.edu:5005/pas37/containers/python36
ARG TOP

RUN yum install -y mariadb-libs && yum clean all 
ADD release.tar /

WORKDIR $TOP
ENTRYPOINT ["venv/bin/gunicorn", "--bind=0.0.0.0:8000", "api.wsgi:app"]

EXPOSE 8000/tcp
VOLUME $TOP/instance
